# About

This is a vanilla javascript experiment expired by a blanket I saw.

It is intended to practice implementing the javascript canvas features without using any framework.

## Current features


The *SphereDrawer* class can be instantiated with a canvas element, an instance of SphereDrawerSkin and an (optional) options object.

1. The *SphereDrawerSkin* is a set of colors that will be used when drawing the spheres.
The first color will be used as a background color. If no background color is provided a random color will be selected from the other colors.

2. The *options* can be passed in a javascript object. The current supported options are:
 * chanceOfNoSphere - **float** the chance that a spot for a sphere will be empty (defaults to 0.01)
 * rememberSpheres - **boolean** whether or the same sphere colors should be used when redrawing the spheres. The spheres are redrawn on resize. (defaults to true)
 * animations - **array** *This is still a work in progress*, however it is intended to be able to pass multiple animations for the spheres. (defaults to an empty array)

### Animations

Resizing the screen will result in a reset of the animations

* SphereWiggleAnimation - makes the spheres wiggle in place. options:
 * wiggleRoomPercentage - (optional) **float** how much the spheres can wiggle, this also reduces the size of the spheres to allow them to move without overlapping.

* SphereBounceAnimation - makes the spheres move in all directions at different speeds. The spheres will bounce off the edges of the screen.
 * maxSpeed - (optional) **float** the maximum speed of the spheres (defaults to 5)
 * minSpeed - (optional) **float** the minimum speed of the spheres (defaults to 0.2)
 * randomXSpeed - (optional) **boolean** whether or not the speed should be calculated randomly (defaults to true)
 * randomYSpeed - (optional) **boolean** whether or not the speed should be calculated randomly (defaults to true)
 * xSpeed - (optional) **float** the horizontal speed of the spheres (defaults to 2)
 * ySpeed - (optional) **float** the vertical speed of the spheres (defaults to 2)
