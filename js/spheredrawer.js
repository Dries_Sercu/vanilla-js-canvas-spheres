class SphereDrawer extends OptionSetter {
	constructor(canvasElement, skin, options) {
		super();
		if (!canvasElement) return;
		if (!skin instanceof SphereDrawerSkin) {
			throw new Error('Please provide a valid SphereDrawerSkin.');
		}
		this.canvas = canvasElement;
		this.spheresList = [];
		this.setSkin(skin);
		this.sphereSkipped = false; // don't skip drawing twice horizontally
		this.context = this.canvas.getContext('2d');
		this.drawingComplete = false;
		this.setOptions(options);

		this.addResizeEventListener();
		this.drawSpheres();
	}

	setSkin(skin, redrawSpheres = false) {
		this.skin = skin;

		if (!this.getCurrentSpheresList()) {
			this.spheresList[skin.getName()] = [];
		}

		if (redrawSpheres) {
			this.redrawSpheres();
		}
	}

	getCurrentSpheresList() {
		return this.spheresList[this.skin.getName()];
	}

	setOptions(options) {
		this.setOption(options, 'chanceOfNoSphere', false, 0.01);
		this.setOption(options, 'rememberSpheres', false, true);
		this.setOption(options, 'animation', false, null);
	}

	setAnimation(animation, redrawSpheres = false) {
		this.setOption({animation: animation}, 'animation', false, null);

		if (redrawSpheres) {
			this.redrawSpheres();
		}
	}

	redrawSpheres() {
		this.clearCanvas();
		this.drawSpheres();
	}

	addResizeEventListener() {
		window.addEventListener('resize', this.drawSpheres.bind(this));
	}

	setCanvasSize() {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	}

	clearCanvas() {
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	stopAllAnimations() {
		this.getCurrentSpheresList().forEach((sphere) => {
			if (sphere) {
				sphere.stopAnimation();
			}
		});
	}

	drawSpheres() {
		this.spheresDrawn = 0;
		this.setCanvasSize();
		this.clearCanvas();
		this.stopAllAnimations();
		this.styleBackground();
		let yStart = this.drawRowsOfCount(1, 10, 0);
		yStart = this.drawRowsOfCount(2, 15, yStart);
		yStart = this.drawRowsOfCount(3, 20, yStart);
		yStart = this.drawRowsOfCount(3, 30, yStart);
		yStart = this.drawRowsOfCount(3, 40, yStart);
		this.drawRowsOfCount(null, 60, yStart, true);
		this.drawingComplete = true;
	}

	drawRowsOfCount(rows, spheresCount, y, fillUntilBottom) {
		let i = 0;
		while ((!fillUntilBottom && i < rows) || (fillUntilBottom)) {
			if (y > this.canvas.height) break;
			y += this.drawRow(spheresCount, y);
			i++;
		}
		return y;
	}

	drawRow(spheresCount, y) {
		const sphereRadius = this.canvas.width / spheresCount / 2;
		y += sphereRadius;
		for (let i = 0; i < spheresCount; i++) {
			const x = i * (sphereRadius * 2) + sphereRadius;
			this.handleSphereDrawing(x, y, sphereRadius);
		}
		return sphereRadius * 2;
	}

	handleSphereDrawing(x, y, radius) {
		if (!this.shouldDrawSphere()) {
			this.skipDrawSphere();
			return;
		}
		this.drawSphere(x, y, radius);
	}

	shouldDrawSphere() {
		if (this.rememberSpheres && this.drawingComplete) {
			const spheresList = this.getCurrentSpheresList();
			if (spheresList[this.spheresDrawn] !== undefined) {
				return spheresList[this.spheresDrawn] !== null;
			}
		}
		return this.sphereSkipped || Math.random() >= this.chanceOfNoSphere;
	}

	skipDrawSphere() {
		this.rememberSphere(null);
		this.sphereSkipped = true;
		this.spheresDrawn++;
	}

	drawSphere(x, y, radius) {
		this.sphereSkipped = false;
		const sphere = new Sphere(this.context, {
			x: x,
			y: y,
			radius: radius,
			fillColor: this.getColorForSphere(),
			animation: this.animation,
			clearCanvasOnAnimation: this.canClearCanvasOnAnimation(this.spheresDrawn),
		});
		this.rememberSphere(sphere);
		this.spheresDrawn++;
	}

	canClearCanvasOnAnimation(sphereCount) {
		if (sphereCount > 2) return false;
		const spheresList = this.getCurrentSpheresList();
		// second sphere, first sphere was skipped
		if (sphereCount === 1 && spheresList[0] === null) return true;
		// third sphere, first and second sphere were skipped
		if (sphereCount === 2 && spheresList[1] === null && spheresList[0] === null) return true;
		return sphereCount === 0;
	}

	getColorForSphere() {
		if (this.rememberSpheres) {
			const spheresList = this.getCurrentSpheresList();

			if (spheresList[this.spheresDrawn]) {
				return spheresList[this.spheresDrawn].getFillColor();
			}
		}
		return this.skin.getRandomSphereColor();
	}

	rememberSphere(sphere) {
		this.getCurrentSpheresList()[this.spheresDrawn] = sphere;
	}

	styleBackground() {
		this.canvas.style.background = this.skin.background;
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = SphereDrawer;
}
