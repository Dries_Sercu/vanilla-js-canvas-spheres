class Controls {
	constructor(controlsElement, drawer, options) {
		this.controlsElement = controlsElement;
		this.drawer = drawer;

		this.collapserElement = controlsElement.getElementsByClassName('collapser')[0];
		this.skinSelectorElement = controlsElement.getElementsByClassName('skinSelector')[0];
		this.animationSelectorElement = controlsElement.getElementsByClassName('animationSelector')[0];

		this.addOptions('skins', this.skinSelectorElement, options.skinSelector);
		this.addOptions('animations', this.animationSelectorElement, options.animationSelector);

		this.sessionStorageNames = {
			skins: 'sphere-skin-index',
			animations: 'sphere-animation-index',
		};

		this.addEventListeners();

		this.setControlsFromSessionStorage();
	}

	addOptions(selectName, selectElement, options) {
		this[selectName] = options;
		options.forEach((option, optionIndex) => {
			const optionElement = document.createElement('option');
			optionElement.value = optionIndex;
			optionElement.text = option.getName();
			selectElement.add(optionElement, null);
		});
	}

	addEventListeners() {
		addEventListenerToHtmlCollection(this.collapserElement, 'click', this.handleCollapse.bind(this), false);
		addEventListenerToHtmlCollection(this.skinSelectorElement, 'change', this.handleSkinChange.bind(this), false);
		addEventListenerToHtmlCollection(this.animationSelectorElement, 'change', this.handleAnimationChange.bind(this), false);
	}

	handleSkinChange(event) {
		this.handleSelectChange(event, this.sessionStorageNames.skins, this.skins, 'setSkin');
	}

	handleAnimationChange(event) {
		this.handleSelectChange(event, this.sessionStorageNames.animations, this.animations, 'setAnimation');
	}

	handleSelectChange(event, sessionStorageName, selectOptions, drawerMethod) {
		window.sessionStorage.setItem(sessionStorageName, event.target.selectedIndex);
		this.drawer[drawerMethod](selectOptions[event.target.selectedIndex], true);
	}

	handleCollapse() {
		if (this.controlsElement.classList.contains('collapsed')) {
			this.controlsElement.classList.remove('collapsed');
			this.collapserElement.textContent = '-';
			return;
		}
		this.controlsElement.classList.add('collapsed');
		this.collapserElement.textContent = '+';
	}

	setControlsFromSessionStorage() {
		this.setControlFromSessionStorage(this.sessionStorageNames.skins, this.skinSelectorElement);
		this.setControlFromSessionStorage(this.sessionStorageNames.animations, this.animationSelectorElement);
	}

	setControlFromSessionStorage(sessionStorageItemName, selectorElement) {
		const sphereSkinIndex = window.sessionStorage.getItem(sessionStorageItemName);
		if (sphereSkinIndex > 0) {
			selectorElement.selectedIndex = sphereSkinIndex;
			selectorElement.dispatchEvent(new Event('change', {'bubbles': true}));
		}
	}
}

/*
 *	Adds eventlistener to every element in htmlCollection
 *  OR add eventlistener to a single element
 */
const addEventListenerToHtmlCollection = function(htmlCollection, eventType, eventHandler, useCapture) {
	useCapture = (useCapture)? useCapture : false;
	if (HTMLCollection.prototype.isPrototypeOf(htmlCollection)) {
		Array.from(htmlCollection).forEach(function(element) {
			element.addEventListener(eventType, eventHandler, useCapture);
		});
		return;
	}
	// htmlCollection is a single element
	htmlCollection.addEventListener(eventType, eventHandler, useCapture);
};


// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = Controls;
}
