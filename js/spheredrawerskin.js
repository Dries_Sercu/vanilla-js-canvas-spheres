class SphereDrawerSkin {
	constructor(name, background, ...sphereColors) {
		if (!name) {
			throw new Error('Please provide a name');
		}
		if (!background) {
			if (sphereColors.length > 1) {
				background = sphereColors.splice(Math.floor(Math.random() * sphereColors.length), 1);
			} else {
				throw new Error('Please provide a background color.');
			}
		}
		if (sphereColors.length === 0) {
			throw new Error('Please provide a sphere color.');
		}
		this.setName(name);
		this.background = background;
		this.colors = sphereColors;
	}

	setName(name) {
		this.name = name;
	}

	getName() {
		return this.name;
	}

	getRandomSphereColor() {
		const randomNumber = Math.floor(Math.random() * this.colors.length);
		return this.colors[randomNumber];
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = SphereDrawerSkin;
}
