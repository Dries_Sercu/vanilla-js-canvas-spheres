class SphereWiggleAnimation extends SphereAnimation {
	setOptions(options) {
		super.setOptions(options);
		this.setOption(options, 'wiggleRoomPercentage', false, 0.02);
	}

	applyChanges(sphere) {
		const wiggleRoom = Math.ceil(sphere.radius * 2 * this.wiggleRoomPercentage);
		// apply wiggle to x
		sphere.x = this.addRandomwigglespace(sphere.originalX, wiggleRoom);
		// apply wiggle to y
		sphere.y = this.addRandomwigglespace(sphere.originalY, wiggleRoom);
		// apply wiggle to radius
		sphere.radius = sphere.originalRadius - wiggleRoom;
	}

	addRandomwigglespace(number, wiggleRoom) {
		const randomWiggleroom = wiggleRoom * Math.random();
		if (Math.random() < 0.5) {
			return number - randomWiggleroom;
		}
		return number + randomWiggleroom;
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = SphereWiggleAnimation;
}
