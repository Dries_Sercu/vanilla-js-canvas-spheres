class SphereBounceAnimation extends SphereAnimation {
	setOptions(options) {
		super.setOptions(options);
		this.setOption(options, 'maxSpeed', false, 5);
		this.setOption(options, 'minSpeed', false, 0.2);
		this.setOption(options, 'randomXSpeed', false, true);
		this.setOption(options, 'randomYSpeed', false, true);
		this.setOption(options, 'xSpeed', false, 2);
		this.setOption(options, 'ySpeed', false, 2);
	}

	applyChanges(sphere) {
		this.setSphereMovement(sphere);
		this.applyMovement(sphere);
	}

	applyMovement(sphere) {
		sphere.x += sphere.xSpeed;
		sphere.y += sphere.ySpeed;
		if (((sphere.x + sphere.radius) > sphere.context.canvas.width && sphere.xSpeed > 0) ||
			((sphere.x - sphere.radius) < 0 && sphere.xSpeed < 0)) {
			sphere.xSpeed = -sphere.xSpeed;
		}
		if (((sphere.y + sphere.radius) > sphere.context.canvas.height && sphere.ySpeed > 0) ||
			((sphere.y - sphere.radius) < 0 && sphere.ySpeed < 0)) {
			sphere.ySpeed = -sphere.ySpeed;
		}
	}

	setSphereMovement(sphere) {
		if (!sphere.xSpeed) {
			sphere.xSpeed = this.getRandomDirection(this.getXSpeed());
		}
		if (!sphere.ySpeed) {
			sphere.ySpeed = this.getRandomDirection(this.getYSpeed());
		}
	}

	getXSpeed() {
		if (this.randomXSpeed) {
			return this.getRandomSpeed();
		}
		return this.xSpeed;
	}

	getYSpeed() {
		if (this.randomYSpeed) {
			return this.getRandomSpeed();
		}
		return this.ySpeed;
	}

	getRandomSpeed() {
		return Math.random() * (this.maxSpeed - this.minSpeed) + this.minSpeed;
	}

	getRandomDirection(number) {
		return (Math.random() < 0.5) ? number : -number;
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = SphereBounceAnimation;
}
