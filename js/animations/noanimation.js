class NoAnimation extends SphereAnimation {
	setOptions(options) {
		super.setOptions(options);
	}

	applyChanges(sphere) {}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = NoAnimation;
}
