class SphereAnimation extends OptionSetter {
	constructor(options) {
		super();
		this.setOptions(options);
	}

	getName() {
		return this.name;
	}

	setOptions(options) {
		this.setOption(options, 'name', true);
	}

	applyChanges(sphere) {
		throw new Error('Please override "applyChanges" in your animation class');
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = SphereAnimation;
}
