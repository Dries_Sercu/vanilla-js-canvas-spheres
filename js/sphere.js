class Sphere extends OptionSetter {
	constructor(context, options) {
		super();
		this.context = context;
		this.continueAnimation = true;

		this.setOptions(options);
		this.originalX = this.x;
		this.originalY = this.y;
		this.originalRadius = this.radius;

		if (this.animation) {
			this.animate();
		}

		this.draw();
	}

	setOptions(options) {
		this.setOption(options, 'x', true);
		this.setOption(options, 'y', true);
		this.setOption(options, 'radius', false, 10);
		this.setOption(options, 'fillColor', true);
		this.setOption(options, 'animation', false, null);
		this.setOption(options, 'clearCanvasOnAnimation', false, false);
	}

	draw() {
		this.context.beginPath();
		this.context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
		this.context.closePath();

		this.context.fillStyle = this.fillColor;
		this.context.fill();
	}

	stopAnimation() {
		this.continueAnimation = false;
	}

	clearCanvas() {
		this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
	}

	animate() {
		if (this.clearCanvasOnAnimation) {
			this.clearCanvas();
		}
		if (this.animation) {
			this.animation.applyChanges(this);
		}
		this.draw();

		if (this.continueAnimation) {
			window.requestAnimationFrame(this.animate.bind(this));
		}
	}

	getFillColor() {
		return this.fillColor;
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = Sphere;
}
