class OptionSetter {
	setOption(options, optionName, required, defaultValue) {
		if (options && options[optionName] !== undefined) {
			this[optionName] = options[optionName];
			return;
		}
		if (required) {
			throw new Error(`Option "${optionName}" not provided.`);
		}
		this[optionName] = defaultValue;
	}
}

// Export node module.
if (typeof module !== 'undefined' && module.hasOwnProperty('exports')) {
	module.exports = OptionSetter;
}
